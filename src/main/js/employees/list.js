'use strict';
import React from 'react'
import ReactDOM from 'react-dom'
import axios from 'axios'

export default class EmployeeList extends React.Component{

	constructor(props) {
		super(props);
		this.state = {employees: []};
	}

	componentDidMount() {
	    axios.get('/api/employees').then((response) => {
	        console.log('The response:' + JSON.stringify(response.data))
	        this.setState({employees: response.data})
	        if(this.props.onReady){
	            this.props.onReady()
	        }
	    }).catch((error) => {
	        throw 'another error! ' + error
	    })
	}

	moveToCreate(e){
	    e.preventDefault()
	    this.props.history.push('/pages/employees/new')
	}

	render() {
		const employees = this.state.employees.map(employee =>
			<Employee key={employee.id} employee={employee}/>
		);
		return (
            <div className="container">
                <div className="card mx-auto mt-5">
                    <div className="card-header">Employees</div>
                    <div className="card-body">
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            {employees}
                        </tbody>
                    </table>
                    <button type="button" className="btn btn-primary" onClick={(e) => {this.moveToCreate(e)}}>Create new employee</button>
                    </div>
                </div>
            </div>
		)
	}
}

class Employee extends React.Component{
	render() {
		return (
			<tr>
				<td scope="row">{this.props.employee.id}</td>
				<td>{this.props.employee.firstName}</td>
				<td>{this.props.employee.lastName}</td>
				<td>{this.props.employee.description}</td>
			</tr>
		)
	}
}