'use strict';
import React from 'react'
import ReactDOM from 'react-dom'
import axios from 'axios'

export default class NewEmployeeForm extends React.Component{

	constructor(props) {
		super(props);
		this.state = {firstName: '', lastName: '', description: ''};
	}

    handleSubmit(e){
        e.preventDefault()
        var bodyFormData = new FormData()
        bodyFormData.append('firstName', this.state.firstName)
        bodyFormData.append('lastName', this.state.lastName)
        bodyFormData.append('description', this.state.description)

        axios({ method: 'post',
                 url: '/api/employees',
                 data: bodyFormData,
                 config: { headers: {'Content-Type': 'multipart/form-data' }}
        })
        .then((response) => {
            this.props.history.push('/pages/employees')
        })
        .catch((error) => {
            console.log(error);
        });
    }

	render() {
		return (
            <div className="container">
                <div className="card mx-auto mt-5">
                    <div className="card-header">Create new Employee</div>
                    <div className="card-body">
                    <form onSubmit={(e) => {this.handleSubmit(e)}}>
                        <div className="form-group">
                            <label htmlFor="firstNameInput">First name</label>
                            <input type="text" className="form-control" id="firstNameInput" placeholder="Enter first name"
                                   value={this.state.firstName} onChange={(e) => this.setState({firstName: e.target.value})} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="lastNameInput">Last name</label>
                            <input type="text" className="form-control" id="lastNameInput" placeholder="Enter last name"
                                    value={this.state.lastName} onChange={(e) => this.setState({lastName: e.target.value})} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="descriptionInput">Description</label>
                            <input type="text" className="form-control" id="descriptionInput" placeholder="Enter description"
                                    value={this.state.description} onChange={(e) => this.setState({description: e.target.value})} />
                        </div>
                      <button type="submit" className="btn btn-primary btn-block">Submit</button>
                    </form>
                    </div>
                </div>
            </div>
		)
	}
}