'use strict';

import React from "react";
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from "react-router-dom";
import EmployeeList from './employees/list';
import NewEmployeeForm from './employees/new';

function App() {
  return (
    <BrowserRouter>
      <div>
        <Route exact path="/pages/employees" component={EmployeeList} />
        <Route path="/pages/employees/new" component={NewEmployeeForm} />
      </div>
    </BrowserRouter>
  );
}
ReactDOM.render(
	<App />,
	document.getElementById('react')
)


