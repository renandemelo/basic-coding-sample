create table hibernate_sequence(
    next_val bigint(20) default null
);

create table employee(
    id bigint(20) not null,
    first_name varchar(255),
    last_name varchar(255),
    description varchar(255),
    primary key(id)
);

insert into hibernate_sequence values(1);