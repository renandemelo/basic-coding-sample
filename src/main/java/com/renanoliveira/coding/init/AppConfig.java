package com.renanoliveira.coding.init;

import com.renanoliveira.coding.domain.service.EmployeeService;
import com.renanoliveira.coding.repository.EmployeeRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    EmployeeService employeeService(EmployeeRepository employeeRepository){
        return new EmployeeService(employeeRepository);
    }
}
