
package com.renanoliveira.coding.init;

import com.renanoliveira.coding.domain.model.Employee;
import com.renanoliveira.coding.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseLoader implements CommandLineRunner {

	@Value("${init-sample-data}")
	private Boolean initSampleData;

	private final EmployeeRepository repository;

	@Autowired
	public DatabaseLoader(EmployeeRepository repository) {
		this.repository = repository;
	}

	@Override
	public void run(String... strings) throws Exception {
		if(initSampleData) {
			Employee employee = new Employee("John", "Snow", "employee of the year");
			this.repository.save(employee);
		}
	}
}