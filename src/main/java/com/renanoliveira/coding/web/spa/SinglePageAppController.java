package com.renanoliveira.coding.web.spa;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SinglePageAppController {

	@RequestMapping(value = {"/pages", "/pages/**"})
	public String index() {
		return "pages";
	}

}