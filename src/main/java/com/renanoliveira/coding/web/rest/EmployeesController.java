package com.renanoliveira.coding.web.rest;

import com.renanoliveira.coding.domain.model.Employee;
import com.renanoliveira.coding.domain.service.EmployeeService;
import com.renanoliveira.coding.web.rest.request.CreateEmployeeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeesController {

	@Autowired
	private EmployeeService employeeService;

	@GetMapping("/api/employees")
	public Iterable<Employee> getEmployees() {
		return employeeService.getAllActiveEmployees();
	}

	@PostMapping("/api/employees")
	public Employee createEmployee(CreateEmployeeRequest request){
		Employee employee = employeeService.create(request.getFirstName(),
								request.getLastName(), request.getDescription());
		return employee;
	}
}