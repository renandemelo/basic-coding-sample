package com.renanoliveira.coding.domain.service;

import com.renanoliveira.coding.domain.model.Employee;
import com.renanoliveira.coding.repository.EmployeeRepository;


public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    public Iterable<Employee> getAllActiveEmployees() {
        return employeeRepository.findAllByActive(true);
    }

    public Employee create(String firstName, String lastName, String description) {
        Employee employee = new Employee(firstName, lastName, description);
        return employeeRepository.save(employee);
    }
}
