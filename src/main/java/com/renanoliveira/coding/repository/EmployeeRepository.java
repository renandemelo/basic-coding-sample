
package com.renanoliveira.coding.repository;

import com.renanoliveira.coding.domain.model.Employee;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    Iterable<Employee> findAllByActive(Boolean active);

}
