import React from 'react';
import ReactDOM from 'react-dom';
import {render, fireEvent, cleanup, waitForElement} from 'react-testing-library'
import EmployeeList from './../../../../src/main/js/employees/list.js'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

describe('EmployeeList', function() {

    let mock
    beforeEach(function() {
        mock = new MockAdapter(axios);
    });

    afterEach(function() {
        mock.restore()
    });

    it('should load the whole list of employees',function(done){
        mock.onGet('/api/employees').reply(200, [
            {
              "id": 1,
              "firstName": "Muenchner",
              "lastName": "Kindl",
              "description": "Not a real person",
              "active": true
            }
          ]);
        const onReady = () => {
            const row = getByText('Muenchner').parentNode
            expect(row.innerText).toBe("1	Muenchner	Kindl	Not a real person");
            done()
        }
        const {getByText, getByTestId, container, asFragment} = render(
            <EmployeeList onReady={onReady} />
        )
    })


});