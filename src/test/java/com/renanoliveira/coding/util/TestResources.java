package com.renanoliveira.coding.util;

import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.nio.file.Files;

public class TestResources {

    public static String resource(String filename) throws IOException {
        return new String(Files.readAllBytes(ResourceUtils.getFile("classpath:" + filename).toPath()));
    }
}
