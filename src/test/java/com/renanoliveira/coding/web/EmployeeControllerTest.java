package com.renanoliveira.coding.web;

import com.renanoliveira.coding.domain.model.Employee;
import com.renanoliveira.coding.domain.service.EmployeeService;
import com.renanoliveira.coding.repository.EmployeeRepository;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import uk.co.datumedge.hamcrest.json.SameJSONAs;

import java.util.Arrays;
import java.util.Optional;

import static com.renanoliveira.coding.util.TestResources.resource;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {

    @MockBean
    private EmployeeService employeeService;

    @Autowired
    private MockMvc mvc;

    @Test
    public void getEmployeesReturnsAllActiveEmployees() throws Exception {
        when(employeeService.getAllActiveEmployees()).thenReturn(asList(
                new Employee(1L,"John", "WithLastName", "Nothing special"),
                new Employee(2L, "Mary","AnotherLastName","Actually special"))
        );
        MvcResult mvcResult = mvc.perform(get("/api/employees"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        String response = mvcResult.getResponse().getContentAsString();
        assertThat(response, SameJSONAs.sameJSONAs(resource("expected-all-employees.json"))
                .allowingAnyArrayOrdering());
    }

    @Test
    public void postOnEmployeesCreatesNewEmployee() throws Exception {
        when(employeeService.create("Peter", "Smith", "A normal person"))
                .thenReturn(new Employee(2L,"Peter", "Smith","A normal person"));
        MvcResult mvcResult = mvc.perform(post("/api/employees")
                .param("firstName","Peter")
                .param("lastName","Smith")
                .param("description","A normal person"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        String response = mvcResult.getResponse().getContentAsString();
        assertThat(response, SameJSONAs.sameJSONAs(resource("expected-new-employee.json"))
                .allowingAnyArrayOrdering());
    }

}
