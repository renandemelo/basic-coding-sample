# How to run

* From the command line (via Maven)
```
./mvnw package
```
and then for running with embedded H2 Database
```
java -jar target/coding-0.0.1-SNAPSHOT.jar
```
or in "production" mode with a local mysql
```
java -jar -Dspring.profiles.active=production target/coding-0.0.1-SNAPSHOT.jar
```

* From the IDE:
```
(after building it once via command line)
Run Application class as Java Application
```
# How to test
```
./mvnw test
```

# Credits
Based on: https://spring.io/guides/tutorials/react-and-spring-data-rest/