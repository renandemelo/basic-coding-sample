var path = require('path');
process.env.CHROME_BIN = require('puppeteer').executablePath()

module.exports = (config) => {
    config.set({

        browsers: ['ChromeHeadless'],

        files: [
          { pattern: 'src/test/js/**/*.js', watched: false },
        ],

        preprocessors: {
          'src/test/js/**/*.js': [ 'webpack', 'sourcemap']
        },

        frameworks: ['jasmine'],

        singleRun: true,

        webpack: {
            devtool: 'inline-source-map',
            mode: 'development',
            module: {
                rules: [
                    {
                        test: path.join(__dirname, '.'),
                        exclude: /(node_modules)/,
                        use: [{
                        loader: 'babel-loader',
                        options: {
                        presets: ["@babel/preset-env", "@babel/preset-react"]
                        }
                    }
                ]
            }
            ]
        },
    },

    webpackMiddleware: {
      // webpack-dev-middleware configuration
      // i. e.
      stats: 'errors-only'
    },

  })
}